package com.porosh.books.network

import com.porosh.books.model.BooksModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

/*
* Need to replace key with actual book api key
* */
const val api_key = "AIzaSyCAVX3C7IY0C1MRvpQqaujQbxqfAsxL4nw"
const val base_url = "https://example.com/api/"

private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

private val retrofit = Retrofit.Builder()
    .baseUrl(base_url)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .build()

interface BookServiceApi {
    @GET
    suspend fun getBookList(@Url endUrl: String): BooksModel
}

object BookApi {
    val bookServiceApi: BookServiceApi by lazy {
        retrofit.create(BookServiceApi::class.java)
    }
}