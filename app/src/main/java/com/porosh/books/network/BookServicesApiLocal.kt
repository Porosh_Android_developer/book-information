package com.porosh.books.network

import com.porosh.books.model.BooksModel
import com.porosh.books.utils.Constants
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

private val moshiLocal = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

val jsonAdapter: JsonAdapter<BooksModel> = moshiLocal.adapter(BooksModel::class.java)
val booksData: BooksModel? = jsonAdapter.fromJson(Constants.JSON_BOOK_DATA)

