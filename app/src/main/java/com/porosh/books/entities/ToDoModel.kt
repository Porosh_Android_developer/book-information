package com.porosh.books.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/*
* This is a demo entity class for uses and customizations
*  */

@Entity(tableName = "tbl_to_do")
data class ToDoModel(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,

    val name: String,
    val priority: String,
    val date: Long,
    val time: Long,

    @ColumnInfo(name="completed")
    var isCompleted: Boolean = false
)