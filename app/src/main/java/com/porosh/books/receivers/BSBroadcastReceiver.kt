package com.porosh.books.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/*
* This is a default demo BroadcastReceiver for uses and customizations
* */

class BSBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

    }

}