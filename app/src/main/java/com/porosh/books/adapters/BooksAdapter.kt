package com.porosh.books.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.porosh.books.databinding.BooksItemRowBinding
import com.porosh.books.model.Data

class BooksAdapter(private val actionCallback: (Data, String) -> Unit): ListAdapter<Data, BooksAdapter.BooksViewHolder>(BooksDiffCallback()) {

    class BooksViewHolder(
        private val binding: BooksItemRowBinding,
        val actionCallback: (Data, String) -> Unit
    ): ViewHolder(binding.root) {
        fun bind(booksData: Data){

            binding.booksDataModel = booksData

            binding.mainRowLL.setOnClickListener {
                actionCallback(booksData,booksData.name)
            }
        }
    }

    class BooksDiffCallback: DiffUtil.ItemCallback<Data>(){
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder {
        val binding = BooksItemRowBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return BooksViewHolder(binding,actionCallback)
    }

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        val items = getItem(position)
        holder.bind(items)
    }

}