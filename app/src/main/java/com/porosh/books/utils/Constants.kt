package com.porosh.books.utils

class Constants {
    companion object {
        const val LOCAL_DATA = "local_data"
        const val CLOUD_DATA = "cloud_data"

        const val JSON_BOOK_DATA = "{\n" +
                "  \"status\": 200,\n" +
                "  \"message\": \"Request success\",\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"id\": 101,\n" +
                "      \"name\": \"Programming Language C\",\n" +
                "      \"image\": \"https://picsum.photos/id/1/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/C_(programming_language)\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 102,\n" +
                "      \"name\": \"The Lord of the Rings\",\n" +
                "      \"image\": \"https://picsum.photos/id/2/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Lord_of_the_Rings\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 103,\n" +
                "      \"name\": \"Pride and Prejudice\",\n" +
                "      \"image\": \"https://picsum.photos/id/3/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Pride_and_Prejudice\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 104,\n" +
                "      \"name\": \"The Hitchhiker's Guide to the Galaxy\",\n" +
                "      \"image\": \"https://picsum.photos/id/4/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 105,\n" +
                "      \"name\": \"The Great Gatsby\",\n" +
                "      \"image\": \"https://picsum.photos/id/5/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Great_Gatsby\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 106,\n" +
                "      \"name\": \"One Hundred Years of Solitude\",\n" +
                "      \"image\": \"https://picsum.photos/id/6/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/One_Hundred_Years_of_Solitude\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 107,\n" +
                "      \"name\": \"Moby Dick\",\n" +
                "      \"image\": \"https://picsum.photos/id/7/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Moby_Dick\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 108,\n" +
                "      \"name\": \"Hamlet\",\n" +
                "      \"image\": \"https://picsum.photos/id/8/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Hamlet\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 109,\n" +
                "      \"name\": \"Crime and Punishment\",\n" +
                "      \"image\": \"https://picsum.photos/id/9/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Crime_and_Punishment\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 110,\n" +
                "      \"name\": \"War and Peace\",\n" +
                "      \"image\": \"https://picsum.photos/id/10/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/War_and_Peace\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 103,\n" +
                "      \"name\": \"Pride and Prejudice\",\n" +
                "      \"image\": \"https://picsum.photos/id/11/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Pride_and_Prejudice\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 104,\n" +
                "      \"name\": \"The Hitchhiker's Guide to the Galaxy\",\n" +
                "      \"image\": \"https://picsum.photos/id/12/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 105,\n" +
                "      \"name\": \"The Great Gatsby\",\n" +
                "      \"image\": \"https://picsum.photos/id/13/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Great_Gatsby\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 106,\n" +
                "      \"name\": \"One Hundred Years of Solitude\",\n" +
                "      \"image\": \"https://picsum.photos/id/14/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/One_Hundred_Years_of_Solitude\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 107,\n" +
                "      \"name\": \"Moby Dick\",\n" +
                "      \"image\": \"https://picsum.photos/id/15/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Moby_Dick\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 108,\n" +
                "      \"name\": \"Hamlet\",\n" +
                "      \"image\": \"https://picsum.photos/id/16/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Hamlet\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 109,\n" +
                "      \"name\": \"Crime and Punishment\",\n" +
                "      \"image\": \"https://picsum.photos/id/17/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Crime_and_Punishment\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 110,\n" +
                "      \"name\": \"War and Peace\",\n" +
                "      \"image\": \"https://picsum.photos/id/18/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/War_and_Peace\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 111,\n" +
                "      \"name\": \"Frankenstein\",\n" +
                "      \"image\": \"https://picsum.photos/id/19/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/Frankenstein\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 112,\n" +
                "      \"name\": \"The Adventures of Sherlock Holmes\",\n" +
                "      \"image\": \"https://picsum.photos/id/21/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Adventures_of_Sherlock_Holmes\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 113,\n" +
                "      \"name\": \"The Odyssey\",\n" +
                "      \"image\": \"https://picsum.photos/id/22/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Odyssey\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 114,\n" +
                "      \"name\": \"The Adventures of Huckleberry Finn\",\n" +
                "      \"image\": \"https://picsum.photos/id/23/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Adventures_of_Huckleberry_Finn\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 115,\n" +
                "      \"name\": \"The Catcher in the Rye\",\n" +
                "      \"image\": \"https://picsum.photos/id/24/300/300\",\n" +
                "      \"url\": \"https://en.wikipedia.org/wiki/The_Catcher_in_the_Rye\"\n" +
                "    }\n" +
                "  ]\n" +
                "}"
    }
}