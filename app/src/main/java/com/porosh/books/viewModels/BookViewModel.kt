package com.porosh.books.viewModels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.porosh.books.model.BooksModel
import com.porosh.books.repos.BookRepository
import com.porosh.books.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(private val repository: BookRepository) : ViewModel() {
    private val TAG = "BookViewModel"

    //private val respository = BookRepository()

    private val _booksData = MutableLiveData<BooksModel>()

    val booksData: LiveData<BooksModel>
        get() = _booksData

    fun fetchBooksData() {
        viewModelScope.launch {
            try {
                /*
                * need to change this Constants.LOCAL_DATA
                * Constants.CLOUD_DATA to call real API
                * */
                _booksData.value = repository.fetchBooksData(Constants.LOCAL_DATA)
            }catch (e: Exception){
                e.localizedMessage?.let { Log.e(TAG, it) }
            }
        }
    }

}