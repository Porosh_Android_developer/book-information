package com.porosh.books.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.porosh.books.entities.ToDoModel
import com.porosh.books.repos.ToDoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ToDoViewModel @Inject constructor(val repository: ToDoRepository): ViewModel() {

    fun insertToDo(toDoModel: ToDoModel){
        viewModelScope.launch {
            repository.insertToDo(toDoModel)
        }
    }

    fun fetchAllToDos(): LiveData<List<ToDoModel>>{
        return repository.getAllToDos()
    }

    fun updateToDo(toDoModel: ToDoModel) {
        toDoModel.isCompleted = !toDoModel.isCompleted
        viewModelScope.launch {
            repository.updateToDo(toDoModel)
        }
    }

    fun deleteToDo(toDoModel: ToDoModel) {
        viewModelScope.launch {
            repository.deleteToDo(toDoModel)
        }
    }

}