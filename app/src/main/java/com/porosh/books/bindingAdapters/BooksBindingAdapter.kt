package com.porosh.books.bindingAdapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("app:setImageResource")
fun setImageResource(iv: ImageView, url: String?) {
    url?.let {
        Glide.with(iv).load(url).into(iv)
    }
}
