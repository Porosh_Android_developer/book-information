package com.porosh.books.repos

import androidx.lifecycle.LiveData
import com.porosh.books.daos.ToDoDao
import com.porosh.books.entities.ToDoModel
import javax.inject.Inject

class ToDoRepository @Inject constructor(val toDoDao: ToDoDao) {

    suspend fun insertToDo(toDoModel: ToDoModel){
        toDoDao.addToDo(toDoModel)
    }

    fun getAllToDos(): LiveData<List<ToDoModel>>{
        return toDoDao.getAllToDos()
    }

    suspend fun updateToDo(toDoModel: ToDoModel) {
        toDoDao.updateToDo(toDoModel)
    }

    suspend fun deleteToDo(toDoModel: ToDoModel) {
        toDoDao.deleteToDo(toDoModel)
    }

}