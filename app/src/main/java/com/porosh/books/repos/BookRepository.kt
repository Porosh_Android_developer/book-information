package com.porosh.books.repos

import com.porosh.books.model.BooksModel
import com.porosh.books.network.BookApi
import com.porosh.books.network.api_key
import com.porosh.books.network.booksData
import com.porosh.books.utils.ApiEndPoints
import com.porosh.books.utils.Constants
import javax.inject.Inject

class BookRepository @Inject constructor() {
    /*
    * create the api end point
    * and call API
    * */
    suspend fun fetchBooksData(dataType: String): BooksModel? {
        val booksModel: BooksModel?
        if (dataType == Constants.CLOUD_DATA){
            val endUrl = "book-list&key=${api_key}"
            booksModel = BookApi.bookServiceApi.getBookList(endUrl)
        }else{
            // call for local data
            booksModel = booksData
        }
        return booksModel
    }
}