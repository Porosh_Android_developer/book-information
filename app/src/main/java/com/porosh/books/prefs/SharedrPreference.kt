package com.porosh.books.prefs

import android.content.Context
import android.content.SharedPreferences

/*
* This is a default demo SharedPreference for uses and customizations
* */

fun getPreference(context: Context) : SharedPreferences = context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE)

fun setData(data: String, editor: SharedPreferences.Editor){
    editor.putString("key",data)
    editor.commit()
}

fun getData(preferences: SharedPreferences): String?{
    val data = preferences.getString("key","")
    return data
}

const val temp_status = "status"
fun setTempStatus(status: Boolean, editor: SharedPreferences.Editor) {
    editor.putBoolean(temp_status, status)
    editor.commit()
}

fun getTempStatus(preferences: SharedPreferences) = preferences.getBoolean(temp_status, false)
