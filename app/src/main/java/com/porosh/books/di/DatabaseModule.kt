package com.porosh.books.di

import android.content.Context
import com.porosh.books.daos.ToDoDao
import com.porosh.books.db.ToDoDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    fun provideToDoDao(@ApplicationContext context: Context): ToDoDao{
        return ToDoDatabase.getDB(context).getToDoDao()
    }

}