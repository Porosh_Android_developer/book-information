package com.porosh.books.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.porosh.books.daos.ToDoDao
import com.porosh.books.entities.ToDoModel

/*
* This is a demo db for uses and customizations
*  */

@Database(entities = [ToDoModel::class], version = 1, exportSchema = false)
abstract class ToDoDatabase : RoomDatabase() {
    abstract fun getToDoDao(): ToDoDao

    companion object {
        private var toDoDatabase: ToDoDatabase? = null

        fun getDB(context: Context): ToDoDatabase {
            return toDoDatabase ?: synchronized(this) {
                val db = Room.databaseBuilder(context, ToDoDatabase::class.java,"ToDoDb")
                    .build()
                toDoDatabase = db
                db
            }
        }
    }

}