package com.porosh.books.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.porosh.books.adapters.BooksAdapter
import com.porosh.books.databinding.FragmentBooksBinding
import com.porosh.books.model.Data
import com.porosh.books.viewModels.BookViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BooksFragment : Fragment() {

    private lateinit var binding: FragmentBooksBinding
    private val bookViewModel: BookViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBooksBinding.inflate(inflater,container,false)

        val adapter = BooksAdapter(::clickAction)
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.adapter = adapter

        bookViewModel.fetchBooksData()
        bookViewModel.booksData.observe(viewLifecycleOwner){
            adapter.submitList(it.data)
        }

        return binding.root
    }

    private fun clickAction(item: Data, name: String){
        Toast.makeText(requireActivity(),"Books Title: $name",Toast.LENGTH_LONG).show()
    }

}