package com.porosh.books.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BooksModel(
    @Json(name = "status")
    var status: Int,

    @Json(name = "message")
    var message: String,

    @Json(name = "data")
    var data: List<Data>
)
